<?php
$_SERVER['REMOTE_ADDR']="127.0.0.1";
$configFile = $argv[2];
require_once __DIR__.'/'.$configFile;
define("OUTPUTCSS", 'output.css');
define("OUTPUTCOMPONENTSDIR", __DIR__.'/output-components');
include('cssparser.php');
$totalCompressBytes = 0;
$totalOriginalBytes = 0;

if($argv[1] == "compress"){
	call_user_func("compress", $configArray);
}

function compress (){
	global $configArray;
	global $totalOriginalBytes;
	global $totalCompressBytes;
	echo "Started filtering components \n" ;
	if(is_file(OUTPUTCSS)) unlink(OUTPUTCSS);
	$componentsDir = __DIR__.'/'.$configArray['general']['component_path'];
	switch($configArray['general']['components_default']) {
		case "enable":
			foreach (new DirectoryIterator($componentsDir) as $fileInfo) {
				if($fileInfo->isDot()) continue;
				$compName = explode('.',$fileInfo->getFilename());
				$compName = $compName[0];
				addComponents($compName, $fileInfo->getPathname(), $configArray['general']['types']);
			}
			break;
		case "disable":
			foreach($configArray['components'] as $key => $val){
				$componentPath = $componentsDir.'/'.$key.'.css';
				addComponents($key, $componentPath, $val);
			}
			break;
	}
	$differenceInBytes = ($totalOriginalBytes - $totalCompressBytes);
	$differenceInPercentage = round((100 * $differenceInBytes) / $totalOriginalBytes, 2);
	echo "\033[1;34m Done adding components \n" ;
	echo "Compressing output file \n" ;
	echo exec('yui-compressor output.css > output.min.css');
	echo "All operations completed! \n" ;
	echo "\033[0;32mOriginal Size : " . $totalOriginalBytes. " bytes \n" ;
	echo "Compressed Size : " . $totalCompressBytes. " bytes \n" ;

	echo "Difference : " . $differenceInBytes . " bytes \n" ;
	echo "Difference percentage : " . $differenceInPercentage . " % \n \033[0m" ;
}


/*
* Adds the requested components to the output based on the condition given
* @param $absDirComps string the absolute path to the component
* @param $condition whether to enable or disable all types in component
*/
function addComponents($compName, $absDirComps, $condition) {
	global $configArray;


	$compContent = file_get_contents($absDirComps);
	if(!is_file(OUTPUTCSS)) touch(OUTPUTCSS);
	$outputContent = file_get_contents(OUTPUTCSS);
	echo "adding ". $compName . "\n\n";
	$typesOutput = addTypes($compName,$compContent,$condition);
	if(!empty($typesOutput)) $outputContent .= $typesOutput ;
	$outputFile = fopen(OUTPUTCSS, "w");
	fwrite($outputFile, $outputContent);
	fclose($outputFile);
}


function addTypes($compName,$compContent, $condition) {
	$includeRule=FALSE;
	$output="";
	global $configArray;
	global $totalOriginalBytes;
	$totalOriginalBytes += strlen($compContent);

	$regex = array(
		"`^([\t\s]+)`ism"=>'',
		"`^\/\*(.+?)\*\/`ism"=>"",
		"`([\n\A;]+)\/\*(.+?)\*\/`ism"=>"$1",
		"`([\n\A;\s]+)//(.+?)[\n\r]`ism"=>"$1\n",
		"`(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+`ism"=>"\n"
	);
	$compContent = preg_replace(array_keys($regex),$regex,$compContent);
	$rulesArray = array();
	if(strpos($compContent, '@') !== FALSE ) {
		$contentArr = explode('@', $compContent);
		for($i=0;$i<sizeof($contentArr);$i++) {
			$thisContent = $contentArr[$i];
			$curlyOpen=0;$curlyClose=0;
			if($i > 0){
				$contentChars="";
				for($j=0; $j < strlen($thisContent); $j++){
					$contentChars .= $thisContent[$j];
					if($thisContent[$j] == '{') {
						$curlyOpen++;
					} else if($thisContent[$j] == '}') {
						$curlyClose++;
					}
					if($curlyClose === $curlyOpen && $curlyOpen !== 0) {
						$rulesArray[] = '@'.$contentChars;
						$contentRemaining = $thisContent;
						$contentRemaining = str_replace ($contentChars ,'' ,$contentRemaining);
						if(!empty($contentRemaining)) {
							$tempArr = explode('}',$contentRemaining);
							foreach($tempArr as $thisBlock) {
								$thisBlock = trim($thisBlock);
								if(!empty($thisBlock)) $rulesArray[] = $thisBlock . '}';
							}
							break;
						}
					}
				}
			} else {
				$tempArray = explode('}',$thisContent);
				foreach($tempArray as &$eachRule) {
					// $eachRule .= '}';
					$eachRule = trim($eachRule);
					if(!empty($eachRule)) $rulesArray[] = $eachRule.'}';
					// print_r($eachRule);
					// echo "\n\n\n";
				}
			}
		}
	} else {
		$tempArraySecond = explode('}',$compContent);
		foreach($tempArraySecond as &$thisBlockTemp) {
			$thisBlockTemp = trim($thisBlockTemp);
			if(!empty($thisBlockTemp)) $rulesArray[] = $thisBlockTemp.'}';
		}
	}
	file_put_contents('testingExplode.txt', print_r($rulesArray, true));

	$includeRule = $condition == 'enable' ? TRUE : FALSE;
	$filterTypes=FALSE;
	foreach($rulesArray as &$rule) {
		if(empty($rule)) continue;
		foreach($configArray['types'] as $typeKey => $compType) {
			if($typeKey == 'default' || $typeKey == $compName){
				$filterTypes=TRUE;
			} else {
				$filterTypes=FALSE;
			}
			if($filterTypes) {
				foreach($compType as $key => $val){
					if(stripos($key,'-') !== FALSE){
						$typesArr = explode('-', $key);
						$ignoreLine=FALSE;
						foreach($typesArr as $combinedType){
							$classPos = stripos($rule,$combinedType );
							if( $classPos === FALSE ){
								$ignoreLine=TRUE;
							}
						}
						if(!$ignoreLine && $val='enable') {
							$includeRule = TRUE ;
						} else {
							$includeRule = FALSE ;
						}
					} else {
						$classPos = stripos($rule,$key);
						if( $classPos !== FALSE && $val=='enable'){
							$includeRule = TRUE ;
						} else if ($classPos !== FALSE && $val=='disable'){
							$includeRule = FALSE ;
						}
					}
				}
			}
		}
		if($includeRule) {
			$lastChar = strlen($rule) - 1;
			$output .= $rule;
			// echo $rule[$lastChar];die();
			// if($rule[$lastChar] !== '}')  $output .= '}';
			// $includeRule = $condition == 'enable' ? TRUE : FALSE;
		}
	}
	$outCompAbs = OUTPUTCOMPONENTSDIR.'/'.$compName.'.css';
	if(!is_file(OUTPUTCSS)) touch(OUTPUTCSS);
	file_put_contents($outCompAbs, $output);
	global $totalCompressBytes;
	$totalCompressBytes += strlen($output);
	return $output;
}
